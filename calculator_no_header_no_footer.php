<?php

/**
	 * Template Name: Calculator - No Header - No Footer
	 *
	 * @package WordPress
	 * @subpackage werkerapp
	 */

	if ( !defined('ABSPATH') ){ die(); }
	
	global $avia_config, $post;

	if ( post_password_required() )
    {
		get_template_part( 'page' ); exit();
    }

function hide_header() {

   $css_to_hide_header = <<<EOT
   <style>
    #header {
        display:none;
    }  
	#header_main {
        display:none;
    }
	#header_bg {
        display:none;
    }
	#main {
		padding-top:0 !important;
	}
	
   </style>
EOT;
   
   echo $css_to_hide_header;
}


function hide_footer() {

   $css_to_hide_footer = <<<EOT
   <style>
    #footer {
        display:none;
    }
   </style>
EOT;
   
   echo $css_to_hide_footer;
}


	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	get_header();
	


	if( false === in_the_loop() )
	{
		/**
		 * To allow other plugins to hook into 'the_content' filter we call this function to set internal WP variables as we do on non ALB templates.
		 * Performs a call to setup_postdata().
		 */
		the_post();
	}
	else
	{
		/**
		 * This is for a fallback only
		 */
		setup_postdata( $post );
	}
	 

	//check if we want to display breadcumb and title
	if( get_post_meta(get_the_ID(), 'header', true) != 'no') echo avia_title();
	 
	do_action( 'ava_after_main_title' );

	if ( isset( $_REQUEST['avia_alb_parser'] ) && ( 'show' == $_REQUEST['avia_alb_parser'] ) && current_user_can( 'edit_post', get_the_ID() ) )
	{
		/**
		 * Display the parser info
		 */
		$content = Avia_Builder()->get_shortcode_parser()->display_parser_info();
		
		/**
		 * Allow e.g. codeblocks to hook properly
		 */
		$content = apply_filters( 'avia_builder_precompile', $content );
		
		Avia_Builder()->get_shortcode_parser()->set_builder_save_location( 'none' );
		$content = ShortcodeHelper::clean_up_shortcode( $content, 'balance_only' );
		ShortcodeHelper::$tree = ShortcodeHelper::build_shortcode_tree( $content );
	}
	else if( ! is_preview() )
	{
		/**
		 * Filter the content for content builder elements
		 */
		$content = apply_filters( 'avia_builder_precompile', get_post_meta( get_the_ID(), '_aviaLayoutBuilderCleanData', true ) );
	}
	else 
	{
		/**
		 * If user views a preview we must use the content because WordPress doesn't update the post meta field
		 */
		$content = apply_filters( 'avia_builder_precompile', get_the_content() );
		
		/**
		 * In preview we must update the shortcode tree to reflect the current page structure.
		 * Prior make sure that shortcodes are balanced.
		 */
		Avia_Builder()->get_shortcode_parser()->set_builder_save_location( 'preview' );
		$content = ShortcodeHelper::clean_up_shortcode( $content, 'balance_only' );
		ShortcodeHelper::$tree = ShortcodeHelper::build_shortcode_tree( $content );
	}
	
	 //check first builder element. if its a section or a fullwidth slider we dont need to create the default openeing divs here

	 $first_el = isset(ShortcodeHelper::$tree[0]) ? ShortcodeHelper::$tree[0] : false;
	 $last_el  = !empty(ShortcodeHelper::$tree)   ? end(ShortcodeHelper::$tree) : false;
	 if(!$first_el || !in_array($first_el['tag'], AviaBuilder::$full_el ) )
	 {
        echo avia_new_section(array('close'=>false,'main_container'=>true, 'class'=>'main_color container_wrap_first'));
	 }
	
	$content = apply_filters('the_content', $content);
	$content = apply_filters('avf_template_builder_content', $content);
	echo $content;


	$avia_wp_link_pages_args = apply_filters('avf_wp_link_pages_args', array(
																			'before' =>'<nav class="pagination_split_post">'.__('Pages:','avia_framework'),
														                    'after'  =>'</nav>',
														                    'pagelink' => '<span>%</span>',
														                    'separator'        => ' ',
														                    ));

	wp_link_pages($avia_wp_link_pages_args);

	
	
	//only close divs if the user didnt add fullwidth slider elements at the end. also skip sidebar if the last element is a slider
	if(!$last_el || !in_array($last_el['tag'], AviaBuilder::$full_el_no_section ) )
	{
		$cm = avia_section_close_markup();

		echo "</div>";
		echo "</div>$cm <!-- section close by builder template -->";

		//get the sidebar
		if (is_singular('post')) {
		    $avia_config['currently_viewing'] = 'blog';
		}else{
		    $avia_config['currently_viewing'] = 'page';
		}
		
		get_sidebar();
		
	}
	else
	{
		echo "<div><div>";
		
	}

// global fix for https://kriesi.at/support/topic/footer-disseapearing/#post-427764
if(in_array($last_el['tag'], AviaBuilder::$full_el_no_section ))
{
	avia_sc_section::$close_overlay = "";
}


echo avia_sc_section::$close_overlay;
echo '		</div><!--end builder template-->';
echo '</div><!-- close default .container_wrap element -->';

?>

<script>

jQuery(document).ready(function($){
	
	// --- Pricing Calculator Year ---

	var price_5_year 		= '<?=get_field('price_1-5_year','option'); ?>';
	var price_6_20_year 	= '<?=get_field('price_6-20_year','option'); ?>';
	var price_21_year 		= '<?=get_field('price_21+_year','option'); ?>';

	// set inital (separator = , instead of .)
	$("#calc-result-year").html(new Intl.NumberFormat('de-DE', {minimumFractionDigits:2, maximumFractionDigits: 2}).format(price_5_year * 12) + " €");

	// calculate sum on change
	$('.calculator-year').on('change', function calculateSumYear() {
	  console.log("calculator-year change");
	  var selected 	= $(this).val();
	  var target 	= Number($('.calculator-year option:selected').val());

	  //$(".user-number-year").html(selected);

	  if(target <= "5") {
	      $("#calc-result-year").html(new Intl.NumberFormat('de-DE', {minimumFractionDigits:2, maximumFractionDigits: 2}).format(selected * price_5_year * 12) + " €");
	      //$(".append-year").html(" (1 bis 5 User)");
	  }

	  else if(target >= "6" && target <= "20") {
	      $("#calc-result-year").html(new Intl.NumberFormat('de-DE', {minimumFractionDigits:2, maximumFractionDigits: 2}).format(selected * price_6_20_year * 12) + " €");
	      //$(".append-year").html(" (6 bis 20 User)");
	  }

	  else if(target >= "21") {
	      $("#calc-result-year").html(new Intl.NumberFormat('de-DE', {minimumFractionDigits:2, maximumFractionDigits: 2}).format(selected * price_21_year * 12) + " €");
	      //$(".append-year").html(" (21 User und mehr)");
	  }
	});

	// --- Pricing Calculator Year 20% off ---

	var price_5_year_20_off 		= '<?=get_field('price_1-5_year_20_off','option'); ?>';
	var price_6_20_year_20_off 	= '<?=get_field('price_6-20_year_20_off','option'); ?>';  
	var price_21_year_20_off 	= '<?=get_field('price_21+_year_20_off','option'); ?>'; 

	
	// set inital (separator = , instead of .)
	$("#calc-result-year_20_off").html(new Intl.NumberFormat('de-DE', {minimumFractionDigits:2, maximumFractionDigits: 2}).format(price_5_year_20_off * 12) + " €");

	// calculate sum on change
	$('.calculator-year_20_off').on('change', function calculateSumYear() {

	  var selected 	= $(this).val();
	  var target 	= Number($('.calculator-year_20_off option:selected').val());

	  //$(".user-number-year").html(selected);

	  if(target <= "5") {
	      $("#calc-result-year_20_off").html(new Intl.NumberFormat('de-DE', {minimumFractionDigits:2, maximumFractionDigits: 2}).format(selected * price_5_year_20_off * 12) + " €");
	      //$(".append-year").html(" (1 bis 5 User)");
	  }

	  else if(target >= "6" && target <= "20") {
	      $("#calc-result-year_20_off").html(new Intl.NumberFormat('de-DE', {minimumFractionDigits:2, maximumFractionDigits: 2}).format(selected * price_6_20_year_20_off * 12) + " €");
	      //$(".append-year").html(" (6 bis 20 User)");
	  }

	  else if(target >= "21") {
	      $("#calc-result-year_20_off").html(new Intl.NumberFormat('de-DE', {minimumFractionDigits:2, maximumFractionDigits: 2}).format(selected * price_21_year_20_off * 12) + " €");
	      //$(".append-year").html(" (21 User und mehr)");
	  }
	});
});
</script>

<?php 

get_footer();

hide_header();

hide_footer();