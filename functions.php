<?php

// Add Scripts

function werkerapp_scripts() {
	// Custom theme scripts
	wp_enqueue_script( 'werkerapp-script', get_stylesheet_directory_uri() . '/js/functions.min.js', array( 'jquery' ), '20180530', true );
	wp_enqueue_style( 'enfold-parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'werkerapp-style',
        get_stylesheet_directory_uri() . '/css/styles.css',
        array('enfold-parent-style')
    );
}
add_action( 'wp_enqueue_scripts', 'werkerapp_scripts' );


// Enfold: change blog headlines

remove_filter( 'post-format-standard', 'avia_default_title_filter', 10, 1 );
add_filter( 'post-format-standard', 'avia_default_title_filter', 10, 1 );

function avia_default_title_filter($current_post)
	{
		if(!empty($current_post['title']))
		{
			$heading = is_singular() ? "h1" : "h2";
	
			$output  = "";
			//$output .= "<{$heading} class='post-title entry-title ". avia_offset_class('meta', false). "'>";
			$output .= "<h1 class='post-title entry-title' ".avia_markup_helper(array('context' => 'entry_title','echo'=>false)).">";
			$output .= "	<a href='".get_permalink()."' rel='bookmark' title='". __('Permanent Link:','avia_framework')." ".$current_post['title']."'>".$current_post['title'];
			$output .= "			<span class='post-format-icon minor-meta'></span>";
			$output .= "	</a>";
			$output .= "</h1>";
	
			$current_post['title'] = $output;
		}

		return $current_post;
	}


// Enfold: Remove back link

function jsg_empty_link() {
    $kriesi_at_backlink = "";
    return $kriesi_at_backlink;
}
add_filter("kriesi_backlink", "jsg_empty_link");


//svg logo

add_filter('avf_logo','new_logo_url');
function new_logo_url($use_image){
$use_image 		= "/wp-content/themes/werkerapp/img/logo_werkerapp.svg";
return $use_image;
}


// add favicon etc

add_action('wp_head', 'add_favicons_etc');
function add_favicons_etc() {
	?>
		<link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/werkerapp/img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/werkerapp/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/werkerapp/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="/wp-content/themes/werkerapp/img/favicon/site.webmanifest">
		<link rel="mask-icon" href="/wp-content/themes/werkerapp/img/favicon/safari-pinned-tab.svg" color="#">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="theme-color" content="#ffffff">
	<?php
}


// register additional fonts

add_filter( 'avf_google_heading_font', 'avia_add_heading_font');
function avia_add_heading_font($fonts)
{
$fonts['Roboto'] = 'Roboto:300,400,700';
return $fonts;
}

add_filter( 'avf_google_content_font', 'avia_add_content_font');
function avia_add_content_font($fonts)
{

$fonts['Roboto'] = 'Roboto:300,400,700';
return $fonts;
}


// add google fonts

function wpb_add_google_fonts() {
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400|Roboto:300,400,700', false ); 
}
add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );


// add acf options page

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page('Preise');	
}


// enfold: override templates of content element - files placed in folder 'shortcodes'

add_filter('avia_load_shortcodes', 'avia_include_shortcode_template', 15, 1);
function avia_include_shortcode_template($paths)
{
  $template_url = get_stylesheet_directory();
      array_unshift($paths, $template_url.'/shortcodes/');

  return $paths;
}