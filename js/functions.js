/* Custom JS Functions */
jQuery(document).ready(function($) {

    // howto slider controls
    $('.avia_textblock.howto ol li a').click(function(e){
        e.preventDefault();
        $('.avia_textblock.howto ol li a').removeClass('active');
        $(this).addClass('active');
        $(this).parent().addClass('active');

        var slideNav = $(this).attr('href');
        $('#howto .avia-slideshow-controls a[href="'+slideNav+'"]').trigger('click');
    });
    $('.avia_textblock.howto ol li').click(function(e){
        e.preventDefault();
        $('.avia_textblock.howto ol li').removeClass('active');
        $(this).addClass('active');
    });

    // trigger help text when scrolling stopped for 30sec
    $(function() {
        var $output = $( "#help-link" ),
            scrolling = "<span id='help-text' class='avia_hidden_link_text'>Hilfe?</span>",
            stopped = "<span id='help-text' class='visible'>Hilfe?</span>";

            $( window ).scroll(function() {
                $output.html( scrolling );

                clearTimeout( $.data( this, "scrollCheck" ) );
                $.data( this, "scrollCheck", setTimeout(function() {
                    $output.html( stopped );
                }, 30000) );

            });
    });
});